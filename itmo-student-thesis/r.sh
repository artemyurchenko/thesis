#!/bin/bash

# requires Pygments for the minted package, pip install Pygments

COMPILE_ARGS="-interaction nonstopmode -file-line-error"

if [[ "$1" == "clean" ]]; then
    rm -f bachelor-thesis.{aux,log,bbl,bcf,blg,run.xml,toc,tct,pdf,out}
else
    for i in bachelor-thesis; do
        xelatex $COMPILE_ARGS $i
        biber                 $i
        xelatex $COMPILE_ARGS $i
        xelatex $COMPILE_ARGS $i
    done

    rm -f bachelor-thesis.{aux,log,bbl,bcf,blg,run.xml,toc,tct,out}
fi
